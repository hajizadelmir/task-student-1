package newProject.EH.controller;
import lombok.RequiredArgsConstructor;
import newProject.EH.dto.StudentDto;
import newProject.EH.service.StudentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentController {

    private final StudentService service;
    @GetMapping("/{id}")
    public StudentDto getStudentById(@PathVariable Long id) {
        return service.getStudentById(id);
    }
    @GetMapping
    public List<StudentDto> getAll(){
        return service.findAllStudent();
    }
    @PostMapping
    public StudentDto createStudent(@RequestBody StudentDto dto) {
        return service.createStudent(dto);
    }
    @DeleteMapping
    public void deleteStudent(@PathVariable Long id) {
        service.deleteStudent(id);
    }
    @PutMapping
    public StudentDto updateStudent(@RequestBody StudentDto dto){
        return service.updateStudent(dto);

    }
}
