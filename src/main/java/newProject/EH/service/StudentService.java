package newProject.EH.service;

import newProject.EH.dto.StudentDto;

import java.util.List;

public interface StudentService {
    StudentDto getStudentById(Long id);

    StudentDto createStudent(StudentDto dto);

    void deleteStudent(Long id);

    StudentDto updateStudent(StudentDto dto);

    List<StudentDto> findAllStudent();
}
