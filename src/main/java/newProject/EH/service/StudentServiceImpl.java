package newProject.EH.service;

import lombok.RequiredArgsConstructor;
import newProject.EH.dto.StudentDto;
import newProject.EH.model.Student;
import newProject.EH.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository repository;
    private final ModelMapper mapper;

    @Override
    public StudentDto getStudentById(Long id) {
        Student byId = repository.getById(id);
        StudentDto dto = mapper.map(byId, StudentDto.class);
        return dto;
    }

    @Override
    public StudentDto createStudent(StudentDto dto) {
        Student student = mapper.map(dto, Student.class);
        Student save = repository.save(student);
        return mapper.map(save, StudentDto.class);

    }

    @Override
    public void deleteStudent(Long id) {
        repository.deleteById(id);

    }

    @Override
    public StudentDto updateStudent(StudentDto dto) {
        Student student = repository.findById(dto.getId()).orElseThrow((() -> new RuntimeException("Student not found!!!")));
        student.setName(dto.getName());
        student.setBirthdate(dto.getBirthdate());
        student.setInstitute(dto.getInstitute());
        Student save = repository.save(student);
        return mapper.map(save, StudentDto.class);

    }
    @Override
    public List<StudentDto> findAllStudent() {
        List<Student> all =repository.findAll();
        List<StudentDto> e=all.stream().map(a->mapper.map(a,StudentDto.class)).collect(Collectors.toList());
        return e;
    }



    }
